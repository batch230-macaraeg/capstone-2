const User = require("../models/user.js");
// const Product = require("../models/product.js");

const bcrypt = require("bcrypt");
// const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		 lastName: reqBody.lastName,
		    email: reqBody.email,
		 password: bcrypt.hashsync(reqBody.password, 10)
	})
}